#!/bin/sh

pkill -9 asterisk

echo 'Compiling asterisk' \
&& cd /usr/src/asterisk \
&& /usr/local/bin/compile-ast.sh \
&& echo 'Compiled successfully, starting asterisk now' \
&& /usr/sbin/asterisk -vvvdddF -T -W -U root -p \
&& sleep 3 \
&& /usr/sbin/asterisk -rx 'core show uptime' \
&& echo 'Successfully compiled and started asterisk'
