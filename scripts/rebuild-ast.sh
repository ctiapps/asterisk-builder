#!/bin/sh

pkill -9 asterisk

cd /usr/src/asterisk \
&& make clean > /dev/null 2>&1 \
&& make dist-clean > /dev/null 2>&1 \
&& ./bootstrap.sh \
&& ./configure --with-crypto --with-ssl --with-srtp \
&& /usr/local/bin/compile-ast.sh \
&& make samples \
&& make config \
&& make install-logrotate \
&& /sbin/ldconfig \
&& echo 'Compiled successfully, starting asterisk now' \
&& /usr/sbin/asterisk -vvvdddF -T -W -U root -p \
&& sleep 3 \
&& /usr/sbin/asterisk -rx 'core show uptime' \
&& echo 'Successfully compiled and started asterisk'
