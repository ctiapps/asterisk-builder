# SIP_OUTBOUNDPROXY in chan_sip.c for Asterisk 13

## Requirements

System are based on Docker, to install docker just follow instructions below:

```
curl -sSL https://get.docker.com/ | sh
```

Then visit  [docker-compose page on github](https://github.com/docker/compose/releases) to check last up-to date version (look for stable release, not pre-release or beta) and install docker-compose (update version number below):

```
curl -L https://github.com/docker/compose/releases/download/1.8.1/docker-compose-`uname -s`-`uname -m` \
  > /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose
```

Finally, reboot server to continue:

```
reboot
```

## Rules!

Don't modify content of asterisk folder. Instead mount modified files through docker-compose.yml:

```
volumes:
  - "./src/chan_sip.c:/usr/src/asterisk/channels/chan_sip.c"
```

Then you can build system

## docker-compose.yml

Sample `docker-compose.yml` file

```
version: '2'

services:
  asterisk:
    image: andrius/asterisk-builder
    volumes:
      - "./src/chan_sip.c:/usr/src/asterisk/channels/chan_sip.c"
```

Important that it's possible to build image or to download it

### docker-compose.yml with build option

Asterisk section should contain build option:

```
asterisk:
  build: .
```

Execute `./docker-build.sh` command, that will compile Asterisk and then will compile updated files. At first iteration it will build native asterisk, then will start it with docker-compose and will compile again with modified files (you have to mount them as volume)

### docker-compose.yml with image option

Asterisk section should contain image option

```
asterisk:
  image: andrius/asterisk-builder
```

Execute `./docker-compile.sh` command, it will dowlnoad image from docker hub and then will compile updated files.

## Usage

1. Edit `docker-compose.yml` and in volumes section, specify files you want to re-compile
2. Execute `./docker-compile.sh` so asterisk will get re-compiled. Check compilation logs!
3. If compilation were succesful, start terminal with three tabs: one for asteirsk, one for callee (account that will accept call) and one for caller.
4. Execute scripts in those terinal tabs: `./asterisk-vdr.sh`, `./callee.sh` and `./caller.sh`. After short timeout, caller will send a call to the callee, you will see call logs in asterisk
5. It's possible to get extra logs with `docker-compose tail -f logs asterisk` or if you do want SIP dump, add ngrep or sngrep to the asterisk

# DEMO

![demo-gif](https://bytebucket.org/voice-apps/asterisk-builder/raw/24a7e3df43214873ab1fd0185f6eb9b8cf34038a/docs/images/sip_outbound.gif)