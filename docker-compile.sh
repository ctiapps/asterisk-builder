#!/bin/sh

docker-compose stop asterisk
docker-compose rm -f asterisk
docker-compose up -d asterisk
docker-compose exec asterisk /usr/local/bin/build-ast.sh

