#!/bin/sh

git submodule init
git submodule update

docker-compose stop asterisk
docker-compose rm -f asterisk
docker-compose build --force-rm --pull asterisk || docker-compose pull asterisk
docker-compose up -d asterisk
docker-compose exec asterisk /usr/local/bin/build-ast.sh

